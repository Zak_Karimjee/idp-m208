/**
 * @Author: Zak Karimjee
 * @Date:   29-11-2017
 * @Project: IDP-M208
 * @Filename: pcf.cpp
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 29-11-2017
 */
#include <robot_link.h>
#include <robot_instr.h>
#include "data.h"
#include "drive.h"
#include <iostream>
#include "pcf.h"
#include <bitset> // to display port readings as binary

using namespace std;
extern SensorReadings sensors;
extern PositionData pos;
extern Drive driver;
extern robot_link rlink;
extern int timestamp;

PCF::PCF(int _input_mask, request_instruction _readCommandID, command_instruction _writeCommandID):input_mask(_input_mask), readCommandID(_readCommandID), writeCommandID(_writeCommandID) {
        //this->setOutputs(0);

}

PCF::PCF(){};

int PCF::getInputs(){
        current_setting = (rlink.request(readCommandID));
        return current_setting;
}

bool PCF::setOutputs(int s){
        current_setting = s&0b11111111;
        bitset<8> cs(current_setting|input_mask);
      	cout << "Writing output" << cs.to_string() << endl;

        return rlink.command(writeCommandID,(current_setting|input_mask));
}

bool PCF::setOutputBit(int bit, bool value){
	current_setting |= (value << bit);
	bitset<8> cs(current_setting);
    cout << "Writing output bit" << bit << ":" << value << "  " << cs.to_string() << endl;
	return rlink.command(writeCommandID,(current_setting|input_mask));
}
