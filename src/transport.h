/**
 * @Author: Zak Karimjee <zk254>
 * @Date:   07-11-2017
 * @Project: IDP-M208
 * @Filename: transport.h
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 29-11-2017

    deals with the picking up and putting down of the boxes with the
    actuator system, as well as the circuit ID system
 */

#include <robot_link.h>
#include <robot_instr.h>

#ifndef TRANSPORT
#define TRANSPORT
#define TILTTIME 2000

// some defines for box identification. Algorithm checks if elapsed time for box trigger is below the defined amount. Should be in ascending order of time.
#define T_SHORT_CIRCUIT 5 // 5 milliseconds or less means it's a short circuit
#define T_BOX_ONE 50
#define T_BOX_TWO 300
#define T_BOX_THREE 1500
#define T_TIMEOUT 2000
// anything that's over 2000 will be detected as open circuit. Over 2000 will timeout the box detection so give open circuit.


class Actuator {
private:

int pcfpin;
public:
Actuator();
Actuator(int _pcfpin);

bool extend();     // return true if successful
bool retract();
bool extended;
};


class TiltMotor {
private:
command_instruction commandID;
long starttime;
bool tilting;
public:
TiltMotor(command_instruction _commandID);
TiltMotor();
bool tiltUp();     // return true if successful
bool tiltDown();

bool isUp;
};


enum BoxType {
        BOX_NONE, BOX_SHORT, BOX_OPEN, BOX_ONE, BOX_TWO, BOX_THREE
};

class BoxIdentifier {
private:
int startTimeStamp;
int timeElapsed;
bool testingBox;
public:
BoxType box;
void startTest();
void runTest();
BoxType classifyBox();
void displayBox();
};

#endif
