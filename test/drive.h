/**
 * @Author: Zak Karimjee <zk254>
 * @Date:   07-11-2017
 * @Project: IDP-M208
 * @Filename: drive.h
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 22-11-2017
    functions for driving the robot around.
 */

#include <robot_link.h>
#include <robot_instr.h>

#ifndef DRIVE_DEF
#define DRIVE_DEF

class Motor {
private:
    char currentSpeed;
    char demandSpeed;
    command_instruction commandID;
    request_instruction motorID;
    bool reverseDirection;
    bool previousEncoder;
    bool currentEncoder;
    long encoderCount;
    bool *encoderBool;
public:
    bool setSpeed(char speed);
    char getSpeed();
    void initialize(command_instruction _commandID, request_instruction _motorID, bool _rv, bool* _encoderBool);
    void stop();
    long trackEncoder();
};


class Drive {
private:
    Motor lmot, rmot;
    float wheelSpacing; //space between wheels in m
    float integrator;
    float balance;
    //something to record previous commands to motors.
public:
    long lenc, renc;
    void initialize(Motor, Motor, float);
    int getSpeed();
    int getRotationSpeed();
    int move(char speed, float balance);
    //positive balance moves right wheel faster, meaning left turn
    void stop();
    //something to backtrack through previous commands.
    void runEncoder();
};

struct cmdRecord {
    int timestamp;
    int lspeed;
    int rspeed;
};
#endif
