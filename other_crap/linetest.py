# @Date:   2017-11-06T22:43:22+00:00
# @Last modified time: 2017-11-07T14:41:00+00:00



import matplotlib.pyplot as plt
import numpy as np
y = 0
sety = 1
theta = 0
dtheta = 0
steps = 10000
dt = 0.01
x = 0
e = 0
e_int = 0
e_prev = 0
e_deriv = 0
V = 1
xlist = []
ylist = []
i = 0

## these are the proportional control values to change
Kp = 1
Kd = 1.5
for i in range(steps):
    e_prev = e
    e = y - sety
    e_int += e*dt
    e_deriv = (e-e_prev)/dt
    dtheta = -Kp*e - Kd*e_deriv
    theta += dtheta*dt
    x = x + V*np.cos(theta)*dt
    y = y + V*np.sin(theta)*dt
    xlist.append(x)
    ylist.append(y)

plt.plot(xlist,ylist);
plt.show()
