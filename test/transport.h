/**
 * @Author: Zak Karimjee <zk254>
 * @Date:   07-11-2017
 * @Project: IDP-M208
 * @Filename: transport.h
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 19-11-2017

    deals with the picking up and putting down of the boxes with the
    actuator system, as well as the circuit ID system
 */
 
#include <robot_link.h>
#include <robot_instr.h>

#ifndef TRANSPORT
#define TRANSPORT

class Actuator {
  private:
    command_instruction commandID;
    int pcfpin;
  public:
    Actuator(command_instruction _commandID, int _pcfpin);
    bool extend(); // return true if successful
    bool retract();
    bool extended;
};

class Transport {
  private:
    Actuator tilt;
    Actuator nip; //cheeky nippers
};

enum BoxType{none, shorted, open, B1, B2, B3};
BoxType identifyBox(); // returns the identity of the current box. If microswitch not pressed, presumes no box and thus none.
#endif
