/**
* @Author: Zak Karimjee <zk254>
* @Date:   06-11-2017
* @Project: IDP-M208
* @Filename: main.cpp
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 22-11-2017
*/



#include "main.h"
#include <iostream>
#include <bitset>

using namespace std;


int startspeed = 100;
int btime;
int outdat = 255;
int main(){
	//init robot link
	cout << "Link Address: " << LINK_ADDR << ", Attempting to initialize link" << endl;
	{
		int val;                              // data from microprocessor
		if (!rlink.initialise (LINK_ADDR)) { // setup the link
			cout << "Cannot initialise link" << endl;
			rlink.print_errs("  ");
			return -1;
		}
		val = rlink.request(TEST_INSTRUCTION);
		if (val == TEST_INSTRUCTION_RESULT) {   // check result
			cout << "Test passed" << endl;
		}
		else if (val == REQUEST_ERROR) {
			cout << "Fatal errors on link:" << endl;
			rlink.print_errs();
			return -1;
		}
		else{
			cout << "Test failed (bad value returned)" << endl;
			return -1;                          // error, finish
		}
	}
	InitialiseRobot();
	// drive in a circle = idp complete
	ptime.start();
	while(true){
		timestamp = ptime.read();
		if(timestamp!=prevPollTs){
				prevPollTs = timestamp;
		}
		if(timestamp%CONTROL_PERIOD <=2 && timestamp!=prevControlTs){
			pollSensors();
			controlPeriodMeasured = timestamp-prevControlTs;
			prevControlTs = timestamp;
			/*===================where stuff happens===================*/
			//set the debug LEDs to the timestamp 3 lsbs for demo purposes
			//if box time is non-negative (ie actually returned) print it.
			//btime = identifyBox();
			//cout << btime << endl;
			outdat = sensors.boxSwitch<<1;
			outdat |= sensors.tiltSwitch;
			setLEDs(outdat);
			/*===================end stuff happens===================*/
			printDebug();
		}
		
	}

}


void InitialiseRobot()
{ 
	LeftMotor.initialize(MOTOR_1_GO, MOTOR_1, false, &sensors.wheelLeft);
	RightMotor.initialize(MOTOR_2_GO, MOTOR_2, true, &sensors.wheelRight);
	driver.initialize(LeftMotor, RightMotor, 0.5);
	cout << "Initialized motors" << endl;
}

void printDebug(){
	bitset<8> p1(sensors.port1);
	bitset<8> p2(sensors.port2);
	cout << timestamp <<" || Port 1: " << p1.to_string() << " Port 2: " << p2.to_string()
	<< "Left Encoder: " << driver.lenc << "Right Encoder: " << driver.renc << endl;

}
