/**
* @Author: Zak Karimjee
* @Date:   19-11-2017
* @Project: IDP-M208
* @Filename: transport.cpp
* @Last modified by:   Zak Karimjee
* @Last modified time: 29-11-2017
*/

#include <robot_link.h>
#include <robot_instr.h>
#include "data.h"
#include "pcf.h"
#include "drive.h"
#include <iostream>
#include "transport.h"
#include "sensors.h"

using namespace std;
extern SensorReadings sensors;
extern PositionData pos;
extern Drive driver;
extern robot_link rlink;
extern int timestamp;
extern PCF outport;

bool Actuator::extend() {
	outport.setOutputBit(pcfpin, false);
	extended = true;
	return true;
}

bool Actuator::retract() {
	outport.setOutputBit(pcfpin, true);
	extended = false;
	return true;
}


bool TiltMotor::tiltUp(){
	if(tilting) {
		// calculate the time it's been going for
		int time_elapsed = timestamp - starttime;
		cout << "TILTING " << time_elapsed << endl;
		if((time_elapsed>TILTTIME) | sensors.tiltSwitch) {
			// if it hits the switch or times out, stop winding.
			cout << "======TILTED BACK======" << endl;
			tilting = false;
			isUp = true;
			rlink.command(commandID,0);
			return true;
		}
		else{
			return false;
		}

	}
	else
	{ //begin tilting
		starttime = timestamp;
		rlink.command(commandID, 100);
		cout << "BEGINNING TILTING" << endl;
		tilting = true;
		return false;
	}
}

bool TiltMotor::tiltDown(){
	if(tilting) {
		if(timestamp-starttime>TILTTIME) {
			tilting = false;
			rlink.command(commandID,0);
			return true;
		}
		else{
										return false;
		}
		if(sensors.tiltSwitch) starttime = starttime;

	}
	else
	{ //begin tilting
									starttime = timestamp;
									rlink.command(commandID, -20);
									tilting = true;
									return false;
	}
}


Actuator::Actuator(int _pcfpin) : pcfpin(_pcfpin) {
}
Actuator::Actuator() {
}
TiltMotor::TiltMotor(command_instruction _commandID) : commandID(_commandID), starttime(0), tilting(false) {
}
TiltMotor::TiltMotor(){
};
void BoxIdentifier::startTest(){
	startTimeStamp = timestamp;
	writePort3(0b00000010);
	testingBox = true;
}

void BoxIdentifier::runTest(){
	if(testingBox) {
									bool flipped = sensors.boxComparator;
									//calculate elapsed time
									timeElapsed = timestamp - startTimeStamp;
									if((flipped && timeElapsed > 0) || (timeElapsed > T_TIMEOUT)) {
																	box = classifyBox();
																	cout << "========== Box: " << box << "T: " << timeElapsed << "==========" << endl;
																	testingBox = false;
									}

	}
}

BoxType BoxIdentifier::classifyBox(){
	if(timeElapsed<T_SHORT_CIRCUIT) return BOX_SHORT;
	else if(timeElapsed<T_BOX_ONE) return BOX_ONE;
	else if(timeElapsed<T_BOX_TWO) return BOX_TWO;
	else if(timeElapsed<T_BOX_THREE) return BOX_THREE;
	else return BOX_OPEN;


}

void BoxIdentifier::displayBox(){
	int led_output = (this->box)<<2;
	writePort3(led_output);
	//TODO: Replace this with using the pcf class.


}
