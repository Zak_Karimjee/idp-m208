/**
 * @Author: Zak Karimjee <zk254>
 * @Date:   06-11-2017
 * @Project: IDP-M208
 * @Filename: main.cpp
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 29-11-2017
 */



#include "main.h"
#include <iostream>
#include <bitset>

using namespace std;

string action_labels[] = {
	"Stop", "Follow Line", "Drop off", "Pick up", "Turn", "Turntable", "Turn Dropoff", "Drive Straight", "Drive over"
	
	};

int main() {
//init robot link
        if (!initialise_link()) {
                return -1;
        };
        InitialiseRobot();
        ptime.start();
        while (true) {
                timestamp = ptime.read();
                if (timestamp % CONTROL_PERIOD == 0 && timestamp != prevControlTs) {
                        controlPeriodMeasured = timestamp - prevControlTs;
                        prevControlTs = timestamp;
                        // <<<< THIS IS WHERE THE MAGIC HAPPENS BOYS AND GIRLS >>>> //
                        BC.PE.poll(); //poll sensors & update all the position stuff

                        go_home = (timestamp > HOMETIME); // check if it's time to go back to the start
                        if (newActionFlag) {
                                //// get the next action
                                get_next_action();
                                cout << "New Action Pulled from queue: " << next_action << endl;
                                newActionFlag = false;
                                if (next_action==FOLLOW_LINE){
									BC.PE.post_action_route_update();
								}
                        } else if ((BC.PE.is_at_junction()) && (next_action==FOLLOW_LINE)) {
                                cout << "AT JUNCTION " << BC.PE.next_junction << endl;
                                BC.PE.reset_integration_before_action();
                                BC.PE.set_next_actions();
                                newActionFlag = true;
                        }
                        BC.runBot();
                        //BoxID.runTest();
                        if (timestamp % 100 <=1 && timestamp != prevDebugTs) {
                                prevDebugTs = timestamp;
                                printDebug();
                        }
                        //if(timestamp % 5000 < 3){
                        //		BoxID.startTest();
                        //		cout << "========== Beginning Box Test ==========" << endl;
                        //	}
                }

                // <<<< MAGIC OVER FOR THIS LOOP >>>> //



        }


}


void get_next_action(){
        if (!next_actions.empty()) { //CHANGE: while to if because while makes no sense
                next_action = next_actions.front();
                next_actions.pop();
                if (next_action == TURN || next_action == TURN_TURNTABLE || next_action == TURN_DROPOFF) {
                        // if the next action involves a turn, get the next heading needed.
                        next_heading = next_headings.front();
                        next_headings.pop();
                        BC.setTurn();
                }
                else if(next_action == DROPOFF){
						BC.transportState = TILT_BOX;	
					} 
				else if(next_action == PICKUP){
						BC.transportState = DRIVING;
						}
                BC.set_travel_tracking();
        }
        else{
                next_action = FOLLOW_LINE;
        }
        BC.action_complete = false;
}

void InitialiseRobot() {
        BC = BotControl(&sensors.wheelLeft, &sensors.wheelRight);
        nipper = Actuator(0);
        nipper.extend();
        tilt = TiltMotor(MOTOR_3_GO);
        RightMotor.initialize(MOTOR_1_GO, MOTOR_1, false);
        LeftMotor.initialize(MOTOR_2_GO, MOTOR_2, true);
        driver.initialize(LeftMotor, RightMotor, WHEELSPACING);
        outport = PCF(0b11100000, READ_PORT_3, WRITE_PORT_3); // TESTING have implemented class but not using this yet.
        cout << "Initialized motors" << endl;
        destinations.push(3);
        // DEBUG To run drop off action
        next_action = FOLLOW_LINE;
        BC.transportState = TILT_BOX;
        newActionFlag = false;
        current_heading = WEST;
}

bool initialise_link(){
        cout << "Link Address: " << LINK_ADDR << endl;
        {
                int val;        // data from microprocessor
                if (!rlink.initialise(LINK_ADDR)) { // setup the link
                        cout << "Cannot initialise link" << endl;
                        rlink.print_errs("  ");
                        return false;
                }
                val = rlink.request(TEST_INSTRUCTION);
                if (val == TEST_INSTRUCTION_RESULT) { // check result
                        cout << "Test passed" << endl;
                } else if (val == REQUEST_ERROR) {
                        cout << "Fatal errors on link:" << endl;
                        rlink.print_errs();
                        return false;
                } else {
                        cout << "Test failed (bad value returned)" << endl;
                        return false; // error, finish
                }
        }
        return true;
}




void printDebug() {
        //TODO: Write a better debug line that outputs current action, current box, current position & intended destination
        if(timestamp%2500 < 100) {
                cout << "Timestamp, Port1, Port2, Current Action, DSJ, Current Transport Action" << endl;
        }
        bitset<8> p1(sensors.port1);
        bitset<8> p2(sensors.port2);
        // cout << BC.PE.current_theta << endl;

        cout << timestamp << "	" << p1.to_string() << "	" << p2.to_string() << "	";
        cout << action_labels[next_action] << "	" << BC.PE.distance_travelled_since_junction() << "	" << BC.transportState << endl;
        // cout << "	" << getIROffset();
        // if(BC.lost) cout << " Searching for Line" << endl;
        // else cout << " On Line" << endl;
}
