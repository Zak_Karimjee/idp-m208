/**
 * @Author: Zak Karimjee
 * @Date:   22-11-2017
 * @Project: IDP-M208
 * @Filename: layout_map.cpp
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 25-11-2017
 */



//
// Created by h on 14/11/17.
//

#include "layout_map.h"
#include "data.h"


Leg::Leg() {
}

Leg::Leg(Headings leg_direction, int sw_junction, int ne_junction, int length_mm) : leg_direction(leg_direction),
                                                                                    sw_junction(sw_junction),
                                                                                    ne_junction(ne_junction),
                                                                                    length_mm(length_mm) {
}


Junction::Junction() {
}

Junction::Junction(int _joins_N, int _joins_W,
                   int _joins_E, int _joins_S,
                   int x, int y) : joins_N(_joins_N), joins_W(_joins_W), joins_E(_joins_E),
                                                 joins_S(_joins_S), x(x), y(y) {
}


Map::Map() {

    legs[0] = Leg(EAST, 3, 4, 770);
    legs[1] = Leg(EAST, 4, 5, 770);
    legs[2] = Leg(NORTH, 4, 0, 1490);
    legs[3] = Leg(EAST, 1, 0, 770);
    legs[4] = Leg(EAST, 0, 2, 770);

    junctions[0] = Junction(9, 3, 4, 2, 880, 1490);
    junctions[1] = Junction(9, 9, 3, 9, 0, 1490); //TODO: measure exact bend radius
    junctions[2] = Junction(9, 4, 9, 9, 10*(88+77+11), 1490);
    junctions[3] = Junction(9, 9, 0, 9, 0, 0);
    junctions[5] = Junction(9, 1, 9, 9, 880*2, 0);
    junctions[4] = Junction(2, 0, 1, 9, 880, 0);
}
