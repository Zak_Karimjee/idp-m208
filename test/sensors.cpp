/**
 * @Author: Zak Karimjee
 * @Date:   08-11-2017
 * @Project: IDP-M208
 * @Filename: sensors.cpp
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 22-11-2017
 */
#include <robot_link.h>
#include <robot_instr.h>
#include "data.h"
#include "drive.h"
#include <iostream>
#include <bitset> // to display port readings as binary

using namespace std;
extern SensorReadings sensors;
extern PositionData pos;
extern Drive driver;
extern robot_link rlink;
extern int timestamp;

int boxStartTimestamp;
bool testingBox;

void pollSensors() {
    // very dependent on final sensor connections.
    // pins defined at:
    //https://docs.google.com/spreadsheets/d/1KLPd-WwrYQ5Wuz1k0dGOwxykjTgAo1GPCNduTB_Y8ys/edit?usp=sharing
    sensors.port1 = rlink.request(READ_PORT_5);
    sensors.port2 = rlink.request(READ_PORT_3);
    sensors.IR1 = (sensors.port1) & 0x1; // furthest left
    sensors.IR2 = (sensors.port1 >> 1) & 0x1;
    sensors.IR3 = (sensors.port1 >> 2) & 0x1;
    sensors.IR4 = (sensors.port1 >> 3) & 0x1; // furthest right
    sensors.wheelLeft = (sensors.port1 >> 4) & 0x1;
    sensors.wheelRight = (sensors.port1 >> 5) & 0x1;
    sensors.boxSwitch = (sensors.port1 >> 6) & 0x1;
    sensors.tiltSwitch = (sensors.port1 >> 7) & 0x1;

    driver.runEncoder();

}

bool writePort3(int data){
	int mask = 0b11100000; //5-7 are inputs so must write 1.
	data = data | mask;
	return rlink.command(WRITE_PORT_3, data);
}

void setLEDs(int led){
		led = (led&0b111)<<2;
		writePort3(led);
	}


int identifyBox(){
	if(testingBox){
		bool flipped = !((sensors.port2 >> 7)& 0x1);
		cout << "f" << flipped;
		if(flipped)
		{
			int boxtime = timestamp - boxStartTimestamp;
			writePort3(0b00000000);
			return boxtime;
			}
	}
	else{
		if(writePort3(0b00000010)){
			boxStartTimestamp = timestamp;
			testingBox = true;
		}
		return -1;
	}
	return -1;
}


int getStatus() {
    return 0;
}

int getIROffset() {
    int offset = 0;
    switch (sensors.port1) {
        case 0:
            offset = 0;
            break;
        case 2:
            offset = 1;
            break;
        case 3:
            offset = 2;
            break;
        case 1:
            offset = 3;
            break;
        case 4:
            offset = -1;
            break;
        case 12:
            offset = -2;
            break;
        case 8:
            offset = -3;
            break;
    }
    return offset;
}
