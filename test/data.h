/**
 * @Author: Zak Karimjee
 * @Date:   16-11-2017
 * @Project: IDP-M208
 * @Filename: data.h
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 20-11-2017
 */

#ifndef DATA
#define DATA
#define REFSPEED 100
enum Headings {N=0,S=2,E=3,W=1}; // will make relative <> absolute easy
/*
    N0
  W1  E3
    S2
*/

enum Actions {DROPOFF, PICKUP, TURN, TURN_TURNTABLE, DRIVE_OVER};


struct SensorReadings{
  bool IR1, IR2, IR3, IR4;
  int port1, port2;
  int Distance;
  bool wheelLeft, wheelRight;
  bool boxSwitch, tiltSwitch;
};

struct PositionData{
   short Angle; // Net rotated angle as a 16 bit anticlockwise rotation.
   int LineDistance; //distance along the line (mm)
 };

struct MotionData{
	char speed;
	float balance;
 };

#endif
