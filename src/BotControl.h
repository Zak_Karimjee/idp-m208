/**
 * @Author: Zak Karimjee
 * @Date:   16-11-2017
 * @Project: IDP-M208
 * @Filename: BotControl.h
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 29-11-2017
 */


#ifndef BOTCONTROL_DEF
#define BOTCONTROL_DEF

#define INTEGRATOR_LIMIT 5
#define LINE_KI 0
#define LINE_KP 0.3
#define WHEELSPACING 260 // wheel space in mm
#define REFSPEED 75 // REFERENCE SPEED FOR IT TO GO AT AS A MAXIMUM
#define REVERSETIME 3000 // reverse for 1 second after dropping off box.


// to define the direction of a turn
enum turnTypeEnum {
        // defining anticlockwise/left turns as positive
        L_LEFT,
        L_RIGHT,
        U_LEFT,
        U_RIGHT
};

enum transportStateEnum {
        DRIVING, MOVE_NIPPER, TILT_BOX

};


class BotControl {
// action flags
private:
bool performingActionFlag;
bool boxHeld;

//PID variables
float integrator;
float balance;
int fspeed;
turnTypeEnum turnType;

// turn tracking
bool isTurning;
float starting_theta;
float angle_turned();             // current distance through turn (deg)
float distance_since_tracking();

// r tracking
float starting_x;
float starting_y;

// straight line driving (DRIVE_STRAIGHT)
float distanceRemaining;

float startTime;


// line following advanced tracking
int previous_offset;
float lost_turn_direction;
public:
// function declarations
BotControl(bool *left_sensor_ptr, bool *right_sensor_ptr);
BotControl();
PositionEstimation PE;

//box stuff
bool dropBox();             // put down the box
bool grabBox();             // pick up a box

//bot motion

bool searchLine();             // relatively simple subroutine to rotate until a line is found again.
void followLine(int speed);

//functions for turning at junctions
void runTurn();
void setTurn();
void set_travel_tracking();

// pickup/putdown state
transportStateEnum transportState;


//int driveBot(float correction, float speed, float direction);         // drive in a certain direction


//general bot functions
void runBot();                     // do everything that pertains to current action flags etc in a loop
bool action_complete;             // tracks whether turn is ongoing/completed.
bool lost;

};

#endif
