/**
 * @Author: Zak Karimjee
 * @Date:   08-11-2017
 * @Project: IDP-M208
 * @Filename: sensors.cpp
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 20-11-2017
 */
#include <robot_link.h>
#include <robot_instr.h>
#include "data.h"
#include "drive.h"
#include <iostream>
#include <bitset> // to display port readings as binary

using namespace std;
extern SensorReadings sensors;
extern PositionData pos;
extern Drive driver;
extern robot_link rlink;
extern int timestamp;

int getStatus() {
    return 0;
}

int getIROffset() {
    int offset = 0;
    int IRData = sensors.port1 & 0b00001111;
    switch (IRData) {
        case 0:
            offset = 0;
            break;
        case 2:
            offset = 1;
            break;
        case 3:
            offset = 2;
            break;
        case 1:
            offset = 3;
            break;
        case 4:
            offset = -1;
            break;
        case 12:
            offset = -2;
            break;
        case 8:
            offset = -3;
            break;
    }
    return offset;
}

bool writePort3(int data){
	int mask = 0b11100000; //5-7 are inputs so must write 1.
	data = data | mask;
	static int prevdata;
	if(prevdata!=data){
		return rlink.command(WRITE_PORT_3, data);
		prevdata = data;
	}
	else
	{
		return true;
		}
}

void setLEDs(int led){
		led = (led&0b111)<<2;
		writePort3(led);
	}





