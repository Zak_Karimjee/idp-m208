/**
 * @Author: Zak Karimjee
 * @Date:   22-11-2017
 * @Project: IDP-M208
 * @Filename: layout_map.h
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 25-11-2017
 */



//
// Created by h on 14/11/17.
//

#include "data.h"

#ifndef MAP_DEF
#define MAP_DEF


class Leg {
public:
// N,E defined as end_junction, S,W defined as start_junction
    Headings leg_direction;
    int sw_junction;
    int ne_junction;
    int length_mm;

    Leg();

    Leg(Headings leg_direction, int sw_junction, int ne_junction, int length_mm);
};

class Junction {
public:
    int joins_N;     // leg id of junctions
    int joins_W;
    int joins_E;
    int joins_S;
    int x;
    int y;

    Junction();

    Junction(int joins_N, int joins_W, int joins_E, int joins_S, int x, int y);
};

class Map {
public:
    Leg legs[5];
    Junction junctions[6];

    Map();
};

#endif
