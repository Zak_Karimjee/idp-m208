/**
 * @Author: Zak Karimjee <zk254>
 * @Date:   07-11-2017
 * @Project: IDP-M208
 * @Filename: drive.cpp
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 22-11-2017
*/
using namespace std;

#include "drive.h"
#include "data.h"
#include <robot_link.h>
#include <robot_instr.h>

extern robot_link rlink;
extern SensorReadings sensors;
extern PositionData pos;
MotionData mo;


void Drive::initialize(Motor _lmot, Motor _rmot, float _wheelspace){
  lmot = _lmot;
  rmot = _rmot;
  wheelSpacing = _wheelspace;
  mo.speed = 0;
  mo.balance = 0.0;

}

int Drive::getSpeed(){
  int speed = (lmot.getSpeed() + rmot.getSpeed())/2;
  return speed;
}

int Drive::getRotationSpeed(){
  // anticlockwise positive
  int omega = (rmot.getSpeed()-lmot.getSpeed())/wheelSpacing;
  return omega;
}

int Drive::move(char speed, float balance){
  rmot.setSpeed(speed*(1+balance));
  lmot.setSpeed(speed*(1-balance));

  // record this information somehow
  mo.balance = balance;
  mo.speed = speed;
  return speed;
}


void Drive::stop(){
	rmot.stop();
	lmot.stop();
	}

void Drive::runEncoder(){
  lenc = lmot.trackEncoder();
  renc = rmot.trackEncoder();
}



void Motor::initialize(command_instruction _commandID, request_instruction _motorID,bool _rv, bool *_encoderBool){
  commandID = _commandID;
  motorID = _motorID;
  rlink.command(RAMP_TIME, 100); // time to ramp to a given speed, 0 being none and 255 full
  reverseDirection = _rv;
  encoderBool = _encoderBool;
}

bool Motor::setSpeed(char speed){
  char rspeed = speed;
  if(reverseDirection){
		rspeed = speed | 0b10000000;
	  }
  bool success = rlink.command(commandID, rspeed);
  demandSpeed = speed;
  return success;
}

char Motor::getSpeed(){
  currentSpeed = rlink.request(motorID);
  return currentSpeed;
}

void Motor::stop(){
		rlink.command(commandID, 0);
	}

long Motor::trackEncoder(){
  previousEncoder = currentEncoder;
  currentEncoder = *encoderBool;
  if(currentEncoder != previousEncoder){
    if((demandSpeed&0b10000000)>>7){
      encoderCount++;
    }
    else{
      encoderCount--;
    }
  }
  return encoderCount;
}
