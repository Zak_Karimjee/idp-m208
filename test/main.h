/**
 * @Author: Zak Karimjee <zk254>
 * @Date:   06-11-2017
 * @Project: IDP-M208
 * @Filename: main.h
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 22-11-2017
    main header file for robot software
 */

#include <robot_instr.h>
#include <robot_link.h>
#include <robot_delay.h>
#include <stopwatch.h>

#include "data.h"
#include "drive.h"
#include "sensors.h"

#define CONTROL_FREQ 50 //Hz
#define CONTROL_PERIOD 20 // milliseconds


#ifdef __arm__
   #define LINK_ADDR "127.0.0.1"
#else
   #define LINK_ADDR 4
#endif





// Definitions of specific robot classes etc


#ifndef MAIN_DEF
#define MAIN_DEF

	// main specific declarations
	stopwatch ptime; // stopwatch for timestamping
	void InitialiseRobot(); // function for setting up motors & other sensor classes etc
	void printDebug(); // standard debug print function that runs every second
	bool newActionFlag;
	bool performingActionFlag;

	// big global things
	Drive driver;
	Motor LeftMotor;
	Motor RightMotor;
	SensorReadings sensors;
	PositionData pos;
	robot_link rlink;

	// main loop specific bits and bobs

	int timestamp; // current timestamp
	int controlPeriodMeasured; // length of control period as measured
	int prevControlTs; // previous timestamp of control loop to prevent rerunning on same Loop
	int prevPollTs; // previous timestamp of polling

	extern MotionData mo;

#endif

#ifdef MAIN_DEF
  // externs for all other files (should be identical to big global things)
  extern Drive driver;
  extern Motor LeftMotor;
  extern Motor RightMotor;
  extern SensorReadings sensors;
  extern PositionData pos;
  extern robot_link rlink;

#endif
