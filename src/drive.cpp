/**
 * @Author: Zak Karimjee <zk254>
 * @Date:   07-11-2017
 * @Project: IDP-M208
 * @Filename: drive.cpp
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 29-11-2017
 */
using namespace std;

#include "drive.h"
#include "data.h"
#include <robot_link.h>
#include <robot_instr.h>
#include <iostream>
#include <cstdlib>

extern robot_link rlink;
extern SensorReadings sensors;
extern PositionData pos;


void Drive::initialize(Motor _lmot, Motor _rmot, float _wheelspace) {
        lmot = _lmot;
        rmot = _rmot;
        wheelSpacing = _wheelspace;

}

int Drive::getSpeed() {
        int speed = (lmot.getSpeed() + rmot.getSpeed()) / 2;
        return speed;
}

int Drive::getRotationSpeed() {
        // anticlockwise positive
        int omega = (rmot.getSpeed() - lmot.getSpeed()) / wheelSpacing;
        return omega;
}

int Drive::move(int speed, float balance) {
        cout << "L" << speed * (1 - balance) << ", R:" << speed * (1 + balance) << endl;
        // fastest wheel should be moving at $speed.
        if(balance > 1.0) balance = 1.0;
        if (balance < -1.0) balance = -1.0;
        float maxval = max(1+balance,1-balance);
        rmot.setSpeed(speed * (1 + balance)/maxval);
        lmot.setSpeed(speed * (1-balance)/maxval);
        return speed;
}

int Drive::spin(int speed, bool spin_ccw) {
        if (!spin_ccw) {
                speed = -speed;
        }
        rmot.setSpeed(speed*1.2);
        lmot.setSpeed(-speed);

        return speed;
}

void Drive::stop() {
        rmot.stop();
        lmot.stop();
}

void Motor::initialize(command_instruction _commandID, request_instruction _motorID, bool _rv) {
        commandID = _commandID;
        motorID = _motorID;
        rlink.command(RAMP_TIME, 100); // time to ramp to a given speed, 0 being none and 255 full
        reverseDirection = _rv;
        //driving_forwards = true;
}

bool Motor::setSpeed(int speed) {
        char rspeed = abs(speed);
		
        if ((speed < 0) != (reverseDirection)) {
                rspeed = rspeed^ 0b1 << 7;
        }
        if(speed != demandSpeed) {
                // only bother sending command if the speed desired hasn't changed.
                demandSpeed = speed;
                return rlink.command(commandID, rspeed);
        }
        else
        {
                return true;
        }
}

char Motor::getSpeed() {
        return demandSpeed;
}

void Motor::stop() {
        rlink.command(commandID, 0);
        
}
Motor::Motor():driving_forwards(false){};

int Motor::getDirection(){
        //returns 1 if going forward, -1 if going backwards
        if(demandSpeed>0) {
			return 1;
		}
        else {
			//cout << "BACK";
			return -1;
			};
}
