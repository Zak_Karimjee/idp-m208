/**
 * @Author: Zak Karimjee
 * @Date:   20-11-2017
 * @Project: IDP-M208
 * @Filename: PositionEstimation.h
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 20-11-2017
 */
//
// Created by h on 14/11/17.
//

#include "layout_map.h"

#ifndef PE_DEF
#define PE_DEF

#define WHEELBASE 260
#define ENCODER_CPR 12
#define WHEEL_DIAMETER_MM 105

class Encoder {
private:
    bool last_state;
    bool *sensor_reading_ptr;
public:
    float distance_travelled();

    Encoder();

    Encoder(bool *sensor_reading_ptr);
};

class PositionEstimation {
private:
    int last_junction;
    void integrate_encoders(int steps);
    bool all_sensors_on();

    Encoder left_encoder;
    Encoder right_encoder;
    Map layout_map;
    

public:
    void poll();
    int next_junction; // last == next at end of action, next from dest or update dest

    void set_next_actions();
    void reset_integration_before_action();
    void post_action_route_update();
    bool is_at_junction();

    //void set_current_leg_id(int leg_id, Headings direction);

    float current_x;
    float current_y;
    float current_theta;
    int current_leg;
    float left_count, right_count;
    float distance_travelled_since_junction();

    PositionEstimation();

    PositionEstimation(bool *left_sensor_ptr, bool *right_sensor_ptr);
};
#endif

void push_turn(Actions type, Headings heading);
