/**
 * @Author: Zak Karimjee <zk254>
 * @Date:   07-11-2017
 * @Project: IDP-M208
 * @Filename: drive.h
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 17-11-2017
    functions for driving the robot around.
 */

#include <robot_link.h>
#include <robot_instr.h>

#ifndef DRIVE_DEF
#define DRIVE_DEF

class Motor {
private:
    int currentSpeed;
    int demandSpeed;
    command_instruction commandID;
    request_instruction motorID;
    bool reverseDirection;
public:
    bool driving_forwards;
    bool setSpeed(int speed);

    char getSpeed();

    void initialize(command_instruction _commandID, request_instruction _motorID, bool _rv);

    void stop();
    
    int getDirection();
    Motor();
};


class Drive {
private:
    //Encoder lenc, renc;
    float wheelSpacing; //space between wheels in m
    float integrator;
    float balance;
    //something to record previous commands to motors.
public:
    Motor lmot, rmot;

    void initialize(Motor, Motor, float);

    int getSpeed();

    int getRotationSpeed();

    int move(int speed, float balance);
    int spin(int speed, bool spin_ccw);

    //positive balance moves right wheel faster, meaning left turn
    void stop();
    //something to backtrack through previous commands.
};

struct cmdRecord {
    int timestamp;
    int lspeed;
    int rspeed;
};
#endif
