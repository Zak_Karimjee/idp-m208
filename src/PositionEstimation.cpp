//
// Created by h on 14/11/17.
//

#include <robot_instr.h>
#include <robot_link.h>
#include <robot_delay.h>
#include <queue>
#include <iostream>
#include "PositionEstimation.h"
#include "layout_map.h"
#include "data.h"
#include <math.h>
#include "drive.h"

extern Motor LeftMotor;
extern Motor RightMotor;

using namespace std;

extern SensorReadings sensors;
extern std::queue<int> destinations;
extern std::queue<Actions> next_actions;
extern std::queue<Headings> next_headings;
extern Actions next_action, current_action;
extern Headings next_heading, current_heading, dropoff_heading;
extern robot_link rlink;
extern Drive driver;

Encoder::Encoder() {};

Encoder::Encoder(bool *sensor_reading_ptr) : sensor_reading_ptr(sensor_reading_ptr) {};


PositionEstimation::PositionEstimation(bool *left_sensor_ptr,
                                       bool *right_sensor_ptr) : last_junction(4),
																 left_encoder(left_sensor_ptr),
                                                                 right_encoder(right_sensor_ptr),
                                                                 layout_map(),
                                                                 next_junction(3),
                                                                 current_x(880),
                                                                 current_y(0),
                                                                 current_theta(-M_PI) {
    destinations.push(3); //pickup first 3
}

PositionEstimation::PositionEstimation() {}


void PositionEstimation::integrate_encoders(int steps = 1) {

	float lchanged = left_encoder.distance_travelled()*driver.lmot.getDirection();
	float rchanged = right_encoder.distance_travelled()*driver.rmot.getDirection();
	// cout << lchanged << "," << rchanged << endl;
    float v_l = lchanged / steps;
    float v_r = rchanged / steps;
    for (int i = 0; i < steps; i++) {
        float du = (v_r + v_l) * 1 / 2; // v is measured in distance/integration_step
        float dtheta = (v_r - v_l) / WHEELBASE;
        current_theta += dtheta;
        current_x += cos(current_theta) * du;
        current_y += sin(current_theta) * du;
    }
    left_count = left_count + lchanged;
    right_count = right_count + rchanged;
}



void PositionEstimation::poll() {
    // very dependent on final sensor connections.
    // pins defined at:
    // https://docs.google.com/spreadsheets/d/1KLPd-WwrYQ5Wuz1k0dGOwxykjTgAo1GPCNduTB_Y8ys/edit?usp=sharing
    sensors.port1 = rlink.request(READ_PORT_5);
    sensors.port2 = rlink.request(READ_PORT_3);
    sensors.IR1 = (sensors.port1) & 0x1; // furthest left
    sensors.IR2 = (sensors.port1 >> 1) & 0x1;
    sensors.IR3 = (sensors.port1 >> 2) & 0x1;
    sensors.IR4 = (sensors.port1 >> 3) & 0x1; // furthest right
    sensors.wheelRight = (sensors.port1 >> 4) & 0x1;
    sensors.wheelLeft = (sensors.port1 >> 5) & 0x1;
    sensors.tiltSwitch = (sensors.port1 >> 6) & 0x1;
    sensors.boxSwitch = (sensors.port1 >> 7) & 0x1;
	sensors.boxComparator = (sensors.port2 >> 7) & 0x1;
	sensors.distanceSensor = rlink.request(ADC4);
	this->integrate_encoders();
}


void PositionEstimation::post_action_route_update() {

    // If at destination
    last_junction = next_junction;
    if (last_junction == destinations.front()) {
        destinations.pop();
    }
    int dest = destinations.front();

    // Set next bit of route
    if (last_junction == 1) { // arriving turntable
		current_heading = EAST;
        next_junction = 0;
	}
    if (last_junction == 2) { 
		current_heading = WEST;
        next_junction = 0;
    }
    
    if (last_junction == 0) { //  leaving turntable
        if (dest == 2) {
            next_junction = dest;
            current_heading = EAST;
        } else if (dest == 1) {
            next_junction = dest;
             current_heading = WEST;
        } else {
            next_junction = 4;
            current_heading = SOUTH;
        }
    }
    
    if (last_junction == 4) { // leaving bottom cross
        if (dest == 5) {
            next_junction = dest;
            current_heading = EAST;
        } else if (dest == 3) {
            next_junction = dest;
             current_heading = WEST;
        } else {
            next_junction = 0;
            current_heading = NORTH;
        }
    }
    if (last_junction == 3) {
		current_heading = EAST;
        next_junction = 4;
	}
    if (last_junction == 5) { // arrivingbottom cross
		current_heading = WEST;
        next_junction = 4;
    }
}

void PositionEstimation::reset_integration_before_action() {
    switch (next_junction) {
        case 1:
        case 2:
        case 3:
        case 5:
            current_y = layout_map.junctions[next_junction].y;
            current_x = layout_map.junctions[next_junction].x;
            break;
        case 0:
            current_x = layout_map.junctions[next_junction].x;
            break;
        case 4:
            current_y = layout_map.junctions[next_junction].y;
            break;
        default: break;
    }
    cout << current_x << "," << current_y << "######" << current_theta << endl;
}

void PositionEstimation::set_next_actions() {
    int dest = destinations.front();
    next_actions.push(STOP);
    switch (last_junction) {
        case 1:
        case 2: // heading to turntable from dropoff
            if ((next_junction == 1) || (next_junction == 2)) {
                next_actions.push(DRIVE_STRAIGHT);
            } else {
                push_turn(TURN_TURNTABLE,SOUTH);
            } break;
        case 3:
        case 5: // heading to center from pickup
            next_actions.push(TURN);
            next_headings.push(NORTH);
            break;
        case 0:
            if ((dest == 1) || (dest == 2)) { // heading to dropoff
                push_turn(TURN, dropoff_heading);
                next_actions.push(DROPOFF);
                if (dest == 1) {
                    push_turn(TURN, EAST);
                } else if (dest == 2) {
                    push_turn(TURN, WEST);
                }
            } else if (dest == 3) {
                push_turn(TURN, WEST);
            } else if (dest == 5) {// heading to square from turntable
                push_turn(TURN, EAST);
            }  break;

        case 4:
			cout << "pushing actions from sqaure to pickup" << dest << endl;
            switch (dest) {
                case 1:
                    push_turn(TURN_TURNTABLE, WEST);
                    break;
                case 2:
                    push_turn(TURN_TURNTABLE, EAST);
                    break;
                case 3:
                    next_actions.push(PICKUP);
                    push_turn(TURN, EAST);
                    break;
                case 5:
                    next_actions.push(PICKUP);
                    push_turn(TURN, WEST);
                    break;

            } break;
    }
}

bool PositionEstimation::is_at_junction() {
	// cout << next_junction << endl;
    switch (next_junction){
        case 0:
            return distance_travelled_since_junction()>1490;
            break;
        case 1:
        case 2:
            return all_sensors_on(); //DONE: implement all_sensors_on
            break;
        case 4:
            return all_sensors_on() || distance_travelled_since_junction()>770; //DONE: implement all_sensors_on
            break;
        case 3:
        case 5:
            // cout << distance_travelled_since_junction();
            return distance_travelled_since_junction()>770; // run recorded turn from first outside of square
            break;
        default:
			return false;
    }
}

bool PositionEstimation::all_sensors_on() {
    return sensors.IR1 && sensors.IR2 && sensors.IR3 && sensors.IR4;
}

float PositionEstimation::distance_travelled_since_junction(){
	// cout << "x position:" << current_x << ',' << layout_map.junctions[last_junction].x << endl;
	// cout << "y position:" << current_y << ',' << layout_map.junctions[last_junction].y << endl;
	

    float dx = current_x-layout_map.junctions[last_junction].x;
    float dy = current_y-layout_map.junctions[last_junction].y;
    return sqrt(pow(dx,2)+pow(dy,2));
}


float Encoder::distance_travelled() {
    if (last_state != *sensor_reading_ptr) {
        last_state = *sensor_reading_ptr;
        return M_PI * WHEEL_DIAMETER_MM / ENCODER_CPR;
    } else {
        return 0;
    }
    
}

void push_turn(Actions type, Headings heading) {
    next_actions.push(type);
    next_headings.push(heading);
}
