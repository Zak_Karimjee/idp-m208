/**
 * @Author: Zak Karimjee <zk254>
 * @Date:   07-11-2017
 * @Project: IDP-M208
 * @Filename: sensors.h
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 19-11-2017

 file for reading and saving all sensor data to sensor flags
 */

#ifndef SENSOR_DEF
#define SENSOR_DEF



bool writePort3(int data);
bool setLEDs(int led);

void pollSensors();

void pollIRSensors();

int getSensorData(int port);

int getStatus();

int getIROffset(); // returns a number from -4 to +4 representing how far across it believes the while line to be (+4 meaning all the way right, -4 being all the way left)

#endif
