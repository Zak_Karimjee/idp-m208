/**
 * @Author: Zak Karimjee <zk254>
 * @Date:   06-11-2017
 * @Project: IDP-M208
 * @Filename: main.h
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 29-11-2017
    main header file for robot software
 */

#include <robot_instr.h>
#include <robot_link.h>
#include <robot_delay.h>
#include <stopwatch.h>
#include <queue>
#include <string>

#include "sensors.h"
#include "data.h"
#include "drive.h"
#include "PositionEstimation.h"
#include "BotControl.h"
#include "transport.h"
#include "pcf.h"

#define CONTROL_FREQ 100 //Hz
#define CONTROL_PERIOD 10
#define HOMETIME 280000 //5 minutes in ms = 5*60*1000 = 300000 minus 20 seconds for getting home.



#ifdef __arm__
#define LINK_ADDR "127.0.0.1"
#else
#define LINK_ADDR 4
#endif





// Definitions of specific robot classes etc


#ifndef MAIN_DEF
#define MAIN_DEF

// main specific declarations
void InitialiseRobot();   // function for setting up motors & other sensor classes etc
void printDebug();   // standard debug print function that runs every second
bool performingActionFlag;
bool initialise_link();
void run_junction_actions();
void get_next_action();

// big global things
stopwatch ptime;   // stopwatch for timestamping
Drive driver;
Motor LeftMotor;
Motor RightMotor;
BoxIdentifier BoxID;
SensorReadings sensors;
PositionData pos;
Actuator nipper;
TiltMotor tilt;
robot_link rlink;
BotControl BC;
PCF outport;

std::queue<Actions> next_actions;
std::queue<Headings> next_headings;
std::queue<int> destinations;
Actions next_action, current_action;
Headings next_heading, current_heading, dropoff_heading;
bool go_home;
bool newActionFlag;


// main loop specific bits and bobs

long timestamp;   // current timestamp
int controlPeriodMeasured;   // length of control period as measured
int prevControlTs;   // previous timestamp of control loop to prevent rerunning on same Loop
int prevDebugTs;   // previous timestamp of debug print


#endif

#ifdef MAIN_DEF
// externs for all other files (should be identical to big global things)
extern stopwatch ptime;   // stopwatch for timestamping
extern Drive driver;
extern Motor LeftMotor;
extern Motor RightMotor;
extern BoxIdentifier BoxID;
extern SensorReadings sensors;
extern PositionData pos;
extern Actuator nipper;
extern TiltMotor tilt;
extern robot_link rlink;
extern BotControl BC;
extern std::queue<Actions> next_actions;
extern std::queue<Headings> next_headings;
extern std::queue<int> destinations;
extern Actions next_action, current_action;
extern Headings next_heading, current_heading, dropoff_heading;
extern bool go_home, newActionFlag;

#endif
