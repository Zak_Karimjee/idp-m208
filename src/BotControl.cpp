/**
 * @Author: Zak Karimjee
 * @Date:   16-11-2017
 * @Project: IDP-M208
 * @Filename: BotControl.cpp
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 29-11-2017
 */

#include <robot_instr.h>
#include <robot_link.h>
#include <robot_delay.h>
#include <stdlib.h>
#include <queue>
#include <cmath>
#include <iostream>
#include "PositionEstimation.h"
#include "data.h"
#include "drive.h"
#include "BotControl.h"
#include "sensors.h"
#include "transport.h"


using namespace std;

extern std::queue<Actions> next_actions;
extern std::queue<Headings> next_headings;
extern Actions next_action, current_action;
extern Headings next_heading, current_heading;
extern SensorReadings sensors;
extern bool newActionFlag;
extern Drive driver;
extern Actuator nipper;
extern TiltMotor tilt;
extern int timestamp;

float BotControl::angle_turned() {
        return PE.current_theta - starting_theta;
}


BotControl::BotControl(bool *left_sensor_ptr,
                       bool *right_sensor_ptr) : PE(left_sensor_ptr,right_sensor_ptr){
};
BotControl::BotControl(){
};

void BotControl::runTurn() {
        // check if turn is finished, if not then continue the turn in whatever way is necessary
        float turn_radius; //desired turn radius in mm

        // set turn radius based on type of turn
        switch(next_action) {
        case TURN:
                turn_radius = 100;
                break;
        case TURN_TURNTABLE:
                turn_radius = 250;
                break;
        case TURN_DROPOFF:
                turn_radius = 0;
                break;
        default:
                turn_radius = 0;
                break;
        }

        float turn_balance;

        if(turn_radius>WHEELSPACING) turn_balance = WHEELSPACING/turn_radius;
        else turn_balance = 1.0;

        switch (turnType) {
        case L_LEFT:
        cout << "L_LEFT" << angle_turned() << endl;
                if (angle_turned() >= M_PI_2) {
                        action_complete = true;
                } else {
                        driver.move(REFSPEED, turn_balance);
                }
                break;
        case L_RIGHT:
                if (angle_turned() <= -M_PI_2) {
                        action_complete = true;
                } else {
                        driver.move(REFSPEED, -1*turn_balance);
                }
                break;
        case U_LEFT:
                if ((angle_turned() >= M_PI) && (sensors.IR2)) {
                        action_complete = true;
                } else {
                        driver.spin(REFSPEED, true);
                }
                break;
        case U_RIGHT:
                //cout << angle_turned() << endl;
                if ((angle_turned() <= -M_PI) && (sensors.IR3)) {
                        action_complete = true;
                } else {
                        driver.spin(REFSPEED/2, false);
                }
                break;

        }


}

void BotControl::runBot() {
        if (action_complete) {
                newActionFlag = true;
        } else {
                switch (next_action) {
                case STOP:
                        driver.stop();
                        newActionFlag = true;
                        cout << "STOP";
                        break;
                case FOLLOW_LINE:
                        //pretty self explanatory tbh
                        if (lost) {
                                this->searchLine();
                        } else {
                                this->followLine(REFSPEED);
                        }
                        break;
                case TURN:
                case TURN_DROPOFF:
                case TURN_TURNTABLE:
                        // keep turning unless runTurn says turn is finished.
                        this->runTurn();
                        break;
                case PICKUP:
                        //grabBox();
                        action_complete = true;
                        break;
                case DROPOFF:
                        //dropBox();
                        action_complete = true;
                        break;
                case DRIVE_OVER:
                // NO BREAK
                case DRIVE_STRAIGHT:
                        if (distance_since_tracking() < 2000000) {
                                driver.move(REFSPEED, 0);
                        } else {
                                action_complete = true;
                        }
                        break;
                }
        }

}
void BotControl::set_travel_tracking() {
        starting_x = PE.current_x;
        starting_y = PE.current_y;

}

float BotControl::distance_since_tracking(){
        float dx = PE.current_x-starting_x;
        float dy = PE.current_y-starting_y;
        return sqrt(pow(dx,2)+pow(dy,2));
}
void BotControl::setTurn() {
        // set current turn type, enable turn flag, and if startNow then begin turn now. Otherwise wait until startTurn called.
        action_complete = false;
        starting_theta = PE.current_theta;
        // calculate relative heading
        {
                // brackets to be able to collapse code
                switch (current_heading) {
                case NORTH:
                        switch (next_heading) {
                        case NORTH:
                                next_action = DRIVE_OVER;
                                break;
                        case EAST:
                                turnType = L_RIGHT;
                                break;
                        case SOUTH:
                                turnType = U_RIGHT;
                                break;
                        case WEST:
                                turnType = L_LEFT;
                                break;
                        }
                        break;
                case EAST:
                        switch (next_heading) {
                        case EAST:
                                next_action = DRIVE_OVER;
                                break;
                        case SOUTH:
                                turnType = L_RIGHT;
                                break;
                        case WEST:
                                turnType = U_RIGHT;
                                break;
                        case NORTH:
                                turnType = L_LEFT;
                                break;
                        }
                        break;
                case SOUTH:
                        switch (next_heading) {
                        case SOUTH:
                                next_action = DRIVE_OVER;
                                break;
                        case WEST:
                                turnType = L_RIGHT;
                                break;
                        case NORTH:
                                turnType = U_RIGHT;
                                break;
                        case EAST:
                                turnType = L_LEFT;
                                break;
                        }
                        break;
                case WEST:
                        switch (next_heading) {
                        case WEST:
                                next_action = DRIVE_OVER;
                                break;
                        case NORTH:
                                turnType = L_RIGHT;
                                break;
                        case EAST:
                                turnType = U_RIGHT;
                                break;
                        case SOUTH:
                                turnType = L_LEFT;
                                break;
                        }
                        break;
                }
        }

}


bool BotControl::searchLine() {
        //spin in a circle until the line sensors show up
        bool lineFound = sensors.IR1 | sensors.IR2 | sensors.IR3 | sensors.IR4;
        if (lineFound) {
                lost = false;
        } else {
                driver.move(20, lost_turn_direction);
        }
        return !lost;
}

void BotControl::followLine(int speed) {
        // This should work pretty well
        float offset = getIROffset();
        previous_offset = offset;
        // cout << "offset" << offset << endl;
        integrator += offset;
        balance = LINE_KP * offset; // + LINE_KI * integrator;
        if (integrator > INTEGRATOR_LIMIT) integrator = INTEGRATOR_LIMIT;
        if (integrator < -INTEGRATOR_LIMIT) integrator = -INTEGRATOR_LIMIT;
        //cout << speed << "," << (1 + abs(offset)) << "," << balance << "HHH" << endl;
        if (abs(offset) > 1) {
                //speed = speed / 2.0;
        }
        driver.move(speed, balance); //slow down if large offset?

        // if you have gone from being all the way on one side to no sensors being read,
        // you have lost the line and need to turn back to it. Set the direction to turn.
        if(offset == 0) {
                if(previous_offset == 3) {
                        this->lost = true;
                        lost_turn_direction = 1.0;
                }
                else if(previous_offset == -3) {
                        this->lost = true;
                        lost_turn_direction = -1.0;
                }

        }
}


bool BotControl::dropBox() {
        // tilt down and drive backwards a box length
        switch(transportState) {
        case DRIVING:
                // move backwards slowly with closed nipper
                driver.move(-REFSPEED/4, 0);
                cout << "REVERSING AWAY FROM BOX" << endl;
                // start timer when the box switch is disconnected
                if(sensors.boxSwitch) startTime = timestamp;
                // if timest
                if(timestamp-startTime > REVERSETIME) {
                        action_complete = true;
                        transportState = TILT_BOX;
                        return true;
                }
                break;
        case TILT_BOX:
                if(tilt.tiltDown()) transportState = DRIVING;
                startTime = timestamp;
                break;
        case MOVE_NIPPER:
                transportState = DRIVING;


        }
        return false;
}

bool BotControl::grabBox() {
        // drive forward until box hit, then nip, then tilt.
        switch(transportState) {
        case DRIVING:

                // move forward slowly with open nipper
                nipper.extend();
                driver.move(REFSPEED/4, 0);
                cout << "Driving towards box" << endl;
                // once the box is hit stop and move to next state
                if(sensors.boxSwitch) {
                        driver.stop();
                        transportState = MOVE_NIPPER;
                }
                break;
        case MOVE_NIPPER:
                // close the nippers.
                nipper.retract();
                transportState = TILT_BOX;
                break;
        case TILT_BOX:
                if(tilt.tiltUp())
                {
                        transportState = DRIVING;
                        action_complete = true;
                        return true;

                }
                break;

        }
        return false;

}
