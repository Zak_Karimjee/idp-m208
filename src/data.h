/**
 * @Author: Zak Karimjee
 * @Date:   16-11-2017
 * @Project: IDP-M208
 * @Filename: data.h
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 25-11-2017
 */

#include <string>
using namespace std;
#ifndef DATA_DEF
#define DATA_DEF

enum Headings {
    NORTH = 0, SOUTH = 2, EAST = 3, WEST = 1
}; // will make relative <> absolute easy
/*
    N0
   W1  E3
    S2
 */

enum Actions {
    STOP, FOLLOW_LINE, DROPOFF, PICKUP, TURN, TURN_TURNTABLE, TURN_DROPOFF, DRIVE_STRAIGHT, DRIVE_OVER
};



struct SensorReadings {
    bool IR1, IR2, IR3, IR4;
    int port1, port2;
    int Distance;
    bool wheelLeft, wheelRight;
    bool boxSwitch, tiltSwitch;
    bool boxComparator;
    int distanceSensor;
};

struct PositionData {
    short Angle; // Net rotated angle as a 16 bit anticlockwise rotation.
    int LineDistance; //distance along the line (mm)
};

#endif
