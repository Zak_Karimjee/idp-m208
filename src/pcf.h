/**
 * @Author: Zak Karimjee
 * @Date:   29-11-2017
 * @Project: IDP-M208
 * @Filename: pcf.h
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 29-11-2017

   Class for managing a PCF I/O port in order to maintain inputs written high so they can be read in & don't sink current unnecessarily


 */


class PCF {
private:
int current_setting;
int input_mask;
request_instruction readCommandID;
command_instruction writeCommandID;

public:
int getInputs();
bool setOutputs(int s);
bool setOutputBit(int bit, bool value);
PCF(int _input_mask, request_instruction _readCommandID, command_instruction _writeCommandID);
PCF();
};
