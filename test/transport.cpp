/**
 * @Author: Zak Karimjee
 * @Date:   19-11-2017
 * @Project: IDP-M208
 * @Filename: transport.cpp
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 19-11-2017
 */

#include "transport.h"
#include <robot_instr.h>


bool Actuator::extend(){
  // waiting to decide on I/O code for pcfs.
    return true;
}

bool Actuator::retract(){
  // waiting to decide on I/O code for pcfs.
  return true;
}

Actuator::Actuator(command_instruction _commandID, int _pcfpin):commandID(_commandID),pcfpin(_pcfpin){}
