/**
* @Author: Zak Karimjee <zk254>
* @Date:   06-11-2017
* @Project: IDP-M208
* @Filename: main.cpp
 * @Last modified by:   Zak Karimjee
 * @Last modified time: 20-11-2017
*/



#include "main.h"
#include <iostream>

using namespace std;


int startspeed = 100;

int main(){
//init robot link
	cout << "Link Address: " << LINK_ADDR << endl;
	{
		int val;                              // data from microprocessor
		if (!rlink.initialise (LINK_ADDR)) { // setup the link
		  cout << "Cannot initialise link" << endl;
		  rlink.print_errs("  ");
		  return -1;
		}
		val = rlink.request(TEST_INSTRUCTION);
		if (val == TEST_INSTRUCTION_RESULT) {   // check result
		  cout << "Test passed" << endl;
		}
		else if (val == REQUEST_ERROR) {
		  cout << "Fatal errors on link:" << endl;
		  rlink.print_errs();
		  return -1;
		}
		else{
		  cout << "Test failed (bad value returned)" << endl;
		return -1;                          // error, finish
		}
	}
	cout << "Type a speed value" << endl;
	cin >> startspeed;
	cout << "Type a Kp value" << endl;
	cin >> line_kp;
	cout << line_kp << endl;
	InitialiseRobot();
	// drive in a circle = idp complete
	ptime.start();
	while(true){
		timestamp = ptime.read();
		PE.poll(); //poll sensors at max rate so nothing is missed
		if(timestamp%CONTROL_PERIOD == 0 && timestamp!=prevControlTs){
			controlPeriodMeasured = timestamp-prevControlTs;
			prevControlTs = timestamp;
			// <<<< THIS IS WHERE THE MAGIC HAPPENS BOYS AND GIRLS >>>> //


			// if there is a new action and not one underway
/*
			newActionFlag = PE.isAtTurn();
			if(newActionFlag && !performingActionFlag){
				current_action = PE.getNextAction();
				BC.processAction();
				performingActionFlag = true;
				// ie. it has processed the new action and is now doing it
				newActionFlag = false;
			}
*/
			// Should call functions in BotControl
			BC.runBot(); // main function in BotControl that does all the things it's been told to do
			// <<<< MAGIC OVER FOR THIS LOOP >>>> //
		}

		if(timestamp%100 == 0 && timestamp!=prevDebugTs){
			prevDebugTs = timestamp;
			printDebug();
			}
		}



}


void InitialiseRobot(){
	PE = PositionEstimation(&sensors.wheelLeft, &sensors.wheelRight);
	LeftMotor.initialize(MOTOR_1_GO, MOTOR_1, false);
	RightMotor.initialize(MOTOR_2_GO, MOTOR_2, true);
	driver.initialize(LeftMotor, RightMotor, 0.5);
	cout << "Initialized motors" << endl;
}

void printDebug(){
	cout <<  timestamp << " Speed: " << int(mo.speed) << "Balance: " << mo.balance << " IR: " << sensors.IR1 << sensors.IR2 << sensors.IR3 << sensors. IR4 << " : " << getIROffset() << "Loop time " << controlPeriodMeasured << endl;
	}
